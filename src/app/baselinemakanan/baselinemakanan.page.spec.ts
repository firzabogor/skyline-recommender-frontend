import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BaselinemakananPage } from './baselinemakanan.page';

describe('BaselinemakananPage', () => {
  let component: BaselinemakananPage;
  let fixture: ComponentFixture<BaselinemakananPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaselinemakananPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BaselinemakananPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
