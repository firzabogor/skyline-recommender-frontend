import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BaselinemakananPageRoutingModule } from './baselinemakanan-routing.module';

import { BaselinemakananPage } from './baselinemakanan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    BaselinemakananPageRoutingModule
  ],
  declarations: [BaselinemakananPage]
})
export class BaselinemakananPageModule {}
