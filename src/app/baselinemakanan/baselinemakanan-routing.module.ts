import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BaselinemakananPage } from './baselinemakanan.page';

const routes: Routes = [
  {
    path: '',
    component: BaselinemakananPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BaselinemakananPageRoutingModule {}
