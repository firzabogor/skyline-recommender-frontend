import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailmakananPageRoutingModule } from './detailmakanan-routing.module';

import { DetailmakananPage } from './detailmakanan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailmakananPageRoutingModule
  ],
  declarations: [DetailmakananPage]
})
export class DetailmakananPageModule {}
