import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailmakananPage } from './detailmakanan.page';

const routes: Routes = [
  {
    path: '',
    component: DetailmakananPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailmakananPageRoutingModule {}
