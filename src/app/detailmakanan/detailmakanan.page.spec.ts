import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailmakananPage } from './detailmakanan.page';

describe('DetailmakananPage', () => {
  let component: DetailmakananPage;
  let fixture: ComponentFixture<DetailmakananPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailmakananPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailmakananPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
