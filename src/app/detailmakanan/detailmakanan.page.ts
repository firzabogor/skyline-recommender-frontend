import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detailmakanan',
  templateUrl: './detailmakanan.page.html',
  styleUrls: ['./detailmakanan.page.scss'],
})
export class DetailmakananPage implements OnInit {
  id:string;
  idc:string;
  data:any;
  rating:any;
  hit:any;
  constructor(
    private route : ActivatedRoute,
    private authService : AuthService,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.idc = params.get('idc');
      console.log(this.idc);
      this.authService.profil_makanan_detail(this.id).subscribe(
        async values => {
          this.data = values['values'];
          document.getElementById("nama").innerHTML = values['values']['nama_restoran'];
          document.getElementById("harga").innerHTML = "Rp." + values['values']['harga'];
          document.getElementById("alamat").innerHTML = values['values']['alamat'];
          document.getElementById("no_telp").innerHTML = values['values']['no_telp'];
          this.hit = values['values']['hit'];
          this.rating = values['values']['rating'];
        },
        async () => {
          const toast = await this.toastCtrl.create({message: 'Connection is bed.', duration: 2000, color: 'dark'});
          await toast.present();
        }
      );
   });
  }

  async pesan(){
    const loading = await this.loadingCtrl.create({message: 'Loading'});
    await loading.present();
    var idc = this.idc;
    var id = this.id;
    var rating = this.rating;
    var hit = this.hit;
    let customer = JSON.parse(localStorage.getItem("customer"));
    this.data['id_customer'] = customer[0]['id'];
    if(idc == '1'){
      this.authService.insertupdatepreferensi(this.data).subscribe(
        async () => {
          loading.dismiss();
          this.router.navigateByUrl('/rating/'+rating+'/'+hit+'/'+id);
        },
        async () => {
          loading.dismiss();
          const toast = await this.toastCtrl.create({message: 'Connection is bed.', duration: 2000, color: 'dark'});
          await toast.present();
        }
      );
    } else if(idc == '2'){
      this.authService.insertupdatepreferensi(this.data).subscribe(
        async () => {
          loading.dismiss();
          this.router.navigateByUrl('/rating/'+rating+'/'+hit+'/'+id);
        },
        async () => {
          loading.dismiss();
          const toast = await this.toastCtrl.create({message: 'Connection is bed.', duration: 2000, color: 'dark'});
          await toast.present();
        }
      );
    } else if(idc == '3'){
      this.authService.insertupdatepreferensi(this.data).subscribe(
        async () => {
          loading.dismiss();
          this.router.navigateByUrl('/rating/'+rating+'/'+hit+'/'+id);
        },
        async () => {
          loading.dismiss();
          const toast = await this.toastCtrl.create({message: 'Connection is bed.', duration: 2000, color: 'dark'});
          await toast.present();
        }
      );
    }
  }

}
