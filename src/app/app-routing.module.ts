import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./auth/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./auth/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./auth/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'preferensi',
    loadChildren: () => import('./preferensi/preferensi.module').then( m => m.PreferensiPageModule)
  },
  {
    path: 'profilmakanan',
    loadChildren: () => import('./profilmakanan/profilmakanan.module').then( m => m.ProfilmakananPageModule)
  },
  {
    path: 'baselinemakanan',
    loadChildren: () => import('./baselinemakanan/baselinemakanan.module').then( m => m.BaselinemakananPageModule)
  },
  {
    //path: 'dashboardcustomer/:myid',
    path: 'dashboardcustomer',
    loadChildren: () => import('./dashboardcustomer/dashboardcustomer.module').then( m => m.DashboardcustomerPageModule)
  },
  {
    path: 'detailmakanan/:idc/:id',
    loadChildren: () => import('./detailmakanan/detailmakanan.module').then( m => m.DetailmakananPageModule)
  },
  {
    path: 'rating/:rating/:hit/:id',
    loadChildren: () => import('./rating/rating.module').then( m => m.RatingPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
