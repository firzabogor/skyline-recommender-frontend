import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(
    private authService: AuthService,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private router: Router
  ) {}

  ngOnInit() {
    var longitude = (<HTMLInputElement>document.getElementById("input-longitude"));
    var latitude = (<HTMLInputElement>document.getElementById("input-latitude"));
    function getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
      } else {
        console.log("Geolocation is not supported by this browser.");
      }
    }

    function showPosition(position) {
      longitude.value = position.coords.longitude;
      latitude.value = position.coords.latitude;
    }
    getLocation();
  }

  form = new FormGroup({
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    type_user: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    longitude: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    latitude: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    kode_asal: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    alamat: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    nama: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    no_telp: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
  });

  async onSubmit(){
    let register = Object.assign({}, this.form.value);
    if (register['username'] == "") {
      const toast = await this.toastCtrl.create({message: 'Please Check Your Input', duration: 2000, color: 'dark'});
      await toast.present();
    } else {
      const loading = await this.loadingCtrl.create({message: 'Registering...'});
      await loading.present();
      this.authService.register(register).subscribe(
      async () => {
        const toast = await this.toastCtrl.create({message: 'User Created', duration: 2000, color: 'dark'});
        await toast.present();
        loading.dismiss();
        this.form.reset();
        this.authService.login(register).subscribe(
          async values => {
            let data = values['data'];
            localStorage.setItem('token', values['token_access']);
            localStorage.setItem('data', JSON.stringify(data));
            loading.dismiss();
            if(register['type_user'] == "c"){
              register['id_user'] = data['id'];
              this.authService.customer(register).subscribe(
                async () => {
                  this.authService.login2(register).subscribe(
                    async values2 => {
                      let data2 = values2['data'];
                      localStorage.setItem('customer', JSON.stringify(data2['customer']));
                      this.router.navigateByUrl('/preferensi');
                    },
                    async () => {

                    }
                  );
                },
                async () => {
                  const alert = await this.alertCtrl.create({message: 'Error ', buttons: ['ok']});
                  await alert.present();
                }
              );
            } else if(register['type_user'] == "r"){
              localStorage.setItem('restoran', JSON.stringify(data['restoran']));
              register['id_user'] = data['id'];
              this.authService.restoran(register).subscribe(
                async () => {
                  this.router.navigateByUrl('/profilmakanan');
                },
                async () => {
                  const alert = await this.alertCtrl.create({message: 'Error ', buttons: ['ok']});
                  await alert.present();
                }
              );
            }
          },
          async () => {
            const alert = await this.alertCtrl.create({message: 'Login Failed ', buttons: ['ok']});
            loading.dismiss();
            await alert.present();
          }
        );
      },
      async () => {
        const alert = await this.alertCtrl.create({message: 'There is an error', buttons: ['ok']});
        loading.dismiss();
        await alert.present();
      });
    }
  }

}
