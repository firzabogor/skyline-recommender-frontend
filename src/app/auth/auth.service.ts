import { User } from './user.model';
import { Preferensi } from './preferensi.model';
import { ProfilMakanan } from './profilmakanan.model';
import { Skyline } from './skyline.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private url = 'http://127.0.0.1:5000';
  constructor(private http: HttpClient){}

  register(user: User){
    return this.http.post(`${this.url}/api/users`, user);
  }

  login(credentials: User): Observable<string>{
    return this.http.post<{values: string}>(`${this.url}/login`, credentials).pipe(
      map(response => response.values)
    )
  }

  login2(credentials: User): Observable<string>{
    return this.http.post<{values: string}>(`${this.url}/login2`, credentials).pipe(
      map(response => response.values)
    )
  }

  username(username: User){
    return this.http.post(`${this.url}/api/users/username`, username);
  }

  restoran(restoran: User){
    return this.http.post(`${this.url}/restoran`, restoran);
  }

  customer(customer: User){
    return this.http.post(`${this.url}/customer`, customer);
  }

  preferensi(preferensi: Preferensi){
    return this.http.post(`${this.url}/baselinepreferensi`, preferensi);
  }

  profilmakanan(profilmakanan: ProfilMakanan){
    return this.http.post(`${this.url}/profilmakanan`, profilmakanan);
  }

  baselinemakanan(baselinemakanan: Preferensi){
    return this.http.post(`${this.url}/baselinemakanan`, baselinemakanan);
  }

  lewati(kode_asal){
    return this.http.get(`${this.url}/wilayah/`+kode_asal);
  }

  skyline(skyline: Skyline){
    return this.http.post(`${this.url}/skyline`, skyline);
  }

  profil_makanan_detail(id){
    return this.http.get(`${this.url}/profilmakanan/`+id);
  }

  insertupdatepreferensi(preferensi: Preferensi){
    return this.http.post(`${this.url}/insertupdatepreferensi`, preferensi);
  }

  rating(data, id){
    return this.http.put(`${this.url}/rating/`+id, data);
  }

  byrating(id){
    return this.http.get(`${this.url}/byrating/`+id);
  }

  bytype(id){
    return this.http.get(`${this.url}/bytype/`+id);
  }
}
