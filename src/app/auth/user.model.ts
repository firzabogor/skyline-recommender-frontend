export interface User {
  id?: string;
  username?: string;
  password?: string;
  email?: string;
  type_user?: string;
  longitude?: string;
  latitude?: string;
  asal?: string;
  alamat?: string;
  nama?: string;
  no_telp?: string;
}
