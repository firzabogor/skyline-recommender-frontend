export interface Skyline {
  id_type_makanan?: string;
  id_customer?: string;
  user_longitude?: number;
  user_latitude?: number;
}
