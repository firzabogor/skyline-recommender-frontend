export interface Preferensi {
  id?: string;
  id_type_makanan?: string;
  id_customer?: string;
  bahan_bumbu?: string;
  tekstur?: string;
  aroma?: string;
  rasa?: string;
  cara_memasak?: string;
  cara_penyajian?: string; 
  level_pedas?: string;
  level_gurih?: string;
  level_manis?: string;
  level_asin?: string;
  level_asam?: string;
  nutrisi?: string;
  tekstur_level?: string;
  penggunaan_bahan_alami?: string;
  kesegaran_makanan?: string;
  kebersihan_makanan?: string;
  status_halal_makanan?: string;
  harga?: string;
}
