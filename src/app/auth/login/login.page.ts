import { Component } from '@angular/core';
import { AuthService } from '../auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  constructor(
    private authService: AuthService,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private router: Router
  ) { }

  form = new FormGroup({
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
  });

  async onSubmit(){
    this.authService.login2(this.form.value).subscribe(
      async values => {
        document.getElementById("buffer-login").style.visibility = "visible";
        let login = Object.assign({}, values);
        localStorage.setItem('token', login['token_access']);
        localStorage.setItem('data', JSON.stringify(login['data']));
        if(login['data']['type_user'] == 'c'){
          localStorage.setItem('customer', JSON.stringify(login['data']['customer']));
          this.router.navigateByUrl('/dashboardcustomer');
        } else if(login['data']['type_user'] == 'r'){
          localStorage.setItem('restoran', JSON.stringify(login['data']['restoran']));
          this.router.navigateByUrl('/profilmakanan');
        }

      },
      async () => {
        const toast = await this.toastCtrl.create({message: 'Username / Password is incorrect', duration: 2000, color: 'dark'});
        await toast.present();
      }
    );
  }

}
