import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profilmakanan',
  templateUrl: './profilmakanan.page.html',
  styleUrls: ['./profilmakanan.page.scss'],
})
export class ProfilmakananPage implements OnInit {

  constructor(
    private authService: AuthService,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private router: Router
  ) { }

  ngOnInit() {
  }

  form = new FormGroup({
    id_type_makanan: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    id_restoran: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    bahan_bumbu: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    tekstur: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    aroma: new FormControl('', [
      Validators.required,
    ]),
    rasa: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    cara_memasak: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    cara_penyajian: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    level_pedas: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    level_gurih: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    level_asam: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    level_manis: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    level_asin: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    nutrisi: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    tekstur_level: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    penggunaan_bahan_alami: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    kesegaran_makanan: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    kebersihan_makanan: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    status_halal_makanan: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    harga: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
  });

  async onSubmit(){
    const loading = await this.loadingCtrl.create({message: 'Logging...'});
    await loading.present();
    let profilmakanan = Object.assign({}, this.form.value);
    profilmakanan['id_restoran'] = localStorage.getItem("id_user");
    console.log(profilmakanan);
    this.authService.profilmakanan(profilmakanan).subscribe(
      async () => {
        loading.dismiss();
        this.router.navigateByUrl('/home');
      },
      async () => {
        const alert = await this.alertCtrl.create({message: 'Failed', buttons: ['ok']});
        loading.dismiss();
        await alert.present();
      },
    );
  }

}
