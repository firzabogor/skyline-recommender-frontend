import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilmakananPageRoutingModule } from './profilmakanan-routing.module';

import { ProfilmakananPage } from './profilmakanan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ProfilmakananPageRoutingModule
  ],
  declarations: [ProfilmakananPage]
})
export class ProfilmakananPageModule {}
