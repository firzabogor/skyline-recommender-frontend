import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProfilmakananPage } from './profilmakanan.page';

describe('ProfilmakananPage', () => {
  let component: ProfilmakananPage;
  let fixture: ComponentFixture<ProfilmakananPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilmakananPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfilmakananPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
