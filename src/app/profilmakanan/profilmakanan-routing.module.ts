import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilmakananPage } from './profilmakanan.page';

const routes: Routes = [
  {
    path: '',
    component: ProfilmakananPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilmakananPageRoutingModule {}
