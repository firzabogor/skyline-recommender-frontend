import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.page.html',
  styleUrls: ['./rating.page.scss'],
})
export class RatingPage implements OnInit {

  id:string;
  rating:any;
  hit:any;
  constructor(
    private route : ActivatedRoute,
    private authService : AuthService,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.rating = params.get('rating');
      this.hit = params.get('hit');

      console.log(params);
   });
  }

  async kirim(){
    var rating = (<HTMLInputElement>document.getElementById("rating")).value;
    var total = (parseInt(this.hit) * parseInt(this.rating) + parseInt(rating));
    var hit = parseInt(this.hit) + 1;
    var rating2 = total / hit;
    var data = {
      'rating': rating2,
      'hit': hit
    };
    this.authService.rating(data, this.id).subscribe(
      async () => {
        this.router.navigateByUrl('/dashboardcustomer');
      },
      async () => {
        const alert = await this.alertCtrl.create({message: 'There is an error', buttons: ['ok']});
        await alert.present();
      }
    );
  }


}
