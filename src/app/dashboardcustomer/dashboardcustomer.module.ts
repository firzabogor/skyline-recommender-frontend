import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardcustomerPageRoutingModule } from './dashboardcustomer-routing.module';

import { DashboardcustomerPage } from './dashboardcustomer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardcustomerPageRoutingModule
  ],
  declarations: [DashboardcustomerPage]
})
export class DashboardcustomerPageModule {}
