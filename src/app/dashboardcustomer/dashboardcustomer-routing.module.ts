import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardcustomerPage } from './dashboardcustomer.page';

const routes: Routes = [
  {
    path: 'dashboardcustomer',
    component: DashboardcustomerPage,
    children: [
      {
        path: 'about',
        loadChildren: () => import('./about/about.module').then( m => m.AboutPageModule)
      },
      {
        path: 'maps',
        loadChildren: () => import('./maps/maps.module').then( m => m.MapsPageModule)
      },
      {
        path: '',
        redirectTo: 'dashboardcustomer/about',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'dashboardcustomer/about',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardcustomerPageRoutingModule {}
