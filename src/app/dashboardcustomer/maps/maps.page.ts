import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
declare var google: any;

@Component({
  selector: 'app-maps',
  templateUrl: './maps.page.html',
  styleUrls: ['./maps.page.scss'],
})
export class MapsPage implements OnInit {

  map:any;
  infoWindow:any;
  nama: string;
  constructor(
    private authService: AuthService,
    private modalController: ModalController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private router: Router
  ) { }

  ngOnInit() {
    let data = JSON.parse(localStorage.getItem("data"));
    this.nama = data['username'];

    this.map = new google.maps.Map(document.getElementById("maps"),{
      center: {lat:-34.397, lng:150.644},
      zoom: 16,
    });

    this.infoWindow = new google.maps.InfoWindow();

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };
          this.map.setCenter(pos);
          this.infoWindow.setPosition(pos);
          this.infoWindow.setContent("Posisi Anda Saat Ini.");
          this.infoWindow.open(this.map);
        }
      );
    } else {
      // Browser doesn't support Geolocation
      console.log("Error");
    }
  }

  async onJenis($event){
    let maps = this.map;
    let id_type_makanan = $event.target.value;
    let id_customer = JSON.parse(localStorage.getItem("customer"))[0]['id'];
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          let user_longitude = position.coords.longitude;
          let user_latitude = position.coords.latitude;

          let data = {
            "id_type_makanan" : id_type_makanan,
            "id_customer" : id_customer,
            "user_longitude" : user_longitude,
            "user_latitude" : user_latitude
          }

          this.authService.skyline(data).subscribe(
            async values => {
              let x;
              let y = 0;
              for(x in values['values']){
                var longitude = values['values'][y]['longitude'];
                var latitude = values['values'][y]['latitude'];
                var position = new google.maps.LatLng(latitude, longitude);
                var locationMarker = new google.maps.Marker({ position, title: values['values'][y]['nama'] });
                locationMarker.setMap(maps);
                y++;
              }
            },
            async () => {
              const toast = await this.toastCtrl.create({message: 'Check your connection.', duration: 2000, color: 'dark'});
              await toast.present();
            }
          );

        });
    }
  }

}
