import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  constructor(
    private authService: AuthService,
    private modalController: ModalController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private router: Router
  ) { }

  ngOnInit() {
  }

  async onJenis($event){
    const loading = await this.loadingCtrl.create({message: 'Loading'});
    await loading.present();
    let id_type_makanan = $event.target.value;
    let id_customer = JSON.parse(localStorage.getItem("customer"))[0]['id'];
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          let user_longitude = position.coords.longitude;
          let user_latitude = position.coords.latitude;

          let data = {
            "id_type_makanan" : id_type_makanan,
            "id_customer" : id_customer,
            "user_longitude" : user_longitude,
            "user_latitude" : user_latitude
          }

          var htmlcode3 = '<ion-card-title style="text-decoration:underline; margin:13px; font-size:22px"><b>Kuliner Terkait</b></ion-card-title>';
          this.authService.bytype(id_type_makanan).subscribe(
            async values => {
              let x;
              let y = 0;
              for(x in values['values']){
                var alamat = values['values'][y]['alamat'];
                var harga = values['values'][y]['harga'];
                var id_profil_makanan = values['values'][y]['id'];
                var nama = values['values'][y]['nama_restoran'];
                var rating = values['values'][y]['rating'];
                y++;

                htmlcode3 += `
                <ion-card>
                  <img src="../../assets/img/mie.jpg" />
                  <ion-card-header>
                    <ion-card-title style="font-size:20px">`+nama+`</ion-card-title>
                    <ion-card-subtitle>Rp.`+harga+`</ion-card-subtitle>
                  </ion-card-header>
                  <ion-card-content style="font-size:16px">
                    <ion-grid style="margin: 0px; padding: 0px">
                      <ion-row style="margin: 0px; padding: 0px; border-bottom: 1px dotted grey">
                        <ion-col style="margin: 0px; padding: 0px" size="12">
                          Rating : </br>`+rating+`
                        </ion-col>
                      </ion-row>
                      <ion-row style="margin: 0px; padding: 0px; border-bottom: 1px dotted grey">
                        <ion-col style="margin: 0px; padding: 0px" size="12">
                        `+alamat+`
                        </ion-col>
                      </ion-row>
                    </ion-grid>
                  </ion-card-content>
                  <ion-footer class="ion-no-border">
                    <ion-toolbar>
                      <center><ion-router-link href="/detailmakanan/3/`+id_profil_makanan+`" class="color">Pilih</ion-router-link></center>
                    </ion-toolbar>
                  </ion-footer>
                </ion-card>`;
              }
              document.getElementById("tekait").innerHTML = htmlcode3;
              var htmlcode2 = '<ion-card-title style="text-decoration:underline; margin:13px; font-size:22px"><b>Kuliner Populer</b></ion-card-title>';
              this.authService.byrating(id_type_makanan).subscribe(
                async values => {
                  let x;
                  let y = 0;

                  htmlcode2 += `
                  <ion-card>
                    <img src="../../assets/img/mie.jpg" />
                    <ion-card-header>
                      <ion-card-title style="font-size:20px">`+nama+`</ion-card-title>
                      <ion-card-subtitle>Rp.`+harga+`</ion-card-subtitle>
                    </ion-card-header>
                    <ion-card-content style="font-size:16px">
                      <ion-grid style="margin: 0px; padding: 0px">
                        <ion-row style="margin: 0px; padding: 0px; border-bottom: 1px dotted grey">
                          <ion-col style="margin: 0px; padding: 0px" size="12">
                            Rating : </br>`+rating+`
                          </ion-col>
                        </ion-row>
                        <ion-row style="margin: 0px; padding: 0px; border-bottom: 1px dotted grey">
                          <ion-col style="margin: 0px; padding: 0px" size="12">
                          `+alamat+`
                          </ion-col>
                        </ion-row>
                      </ion-grid>
                    </ion-card-content>
                    <ion-footer class="ion-no-border">
                      <ion-toolbar>
                        <center><ion-router-link href="/detailmakanan/2/`+id_profil_makanan+`" class="color">Pilih</ion-router-link></center>
                      </ion-toolbar>
                    </ion-footer>
                  </ion-card>`;

                  document.getElementById("populer").innerHTML = htmlcode2;
                  var htmlcode = '<ion-card-title style="text-decoration:underline; margin:13px; font-size:22px"><b>Rekomendasi Kuliner</b></ion-card-title>';
                  this.authService.skyline(data).subscribe(
                    async values => {
                      let x;
                      let y = 0;
                      for(x in values['values']){
                        var longitude = values['values'][y]['longitude'];
                        var latitude = values['values'][y]['latitude'];
                        var distance = values['values'][y]['distance'];
                        var alamat = values['values'][y]['alamat'];
                        var latitude = values['values'][y]['latitude'];
                        var cosine = values['values'][y]['cosine'];
                        var gower = values['values'][y]['gower'];
                        var harga = values['values'][y]['harga'];
                        var id_profil_makanan = values['values'][y]['id_profil_makanan'];
                        var nama = values['values'][y]['nama'];
                        var rating = values['values'][y]['rating'];
                        y++;

                        htmlcode += `
                        <ion-card>
                          <img src="../../assets/img/mie.jpg" />
                          <ion-card-header>
                            <ion-card-title style="font-size:20px">`+nama+`</ion-card-title>
                            <ion-card-subtitle>Rp.`+harga+`</ion-card-subtitle>
                          </ion-card-header>
                          <ion-card-content style="font-size:16px">
                            <ion-grid style="margin: 0px; padding: 0px">
                              <ion-row style="margin: 0px; padding: 0px; border-bottom: 1px dotted grey">
                                <ion-col style="margin: 0px; padding: 0px" size="12">
                                  Rating : </br>`+rating+`
                                </ion-col>
                              </ion-row>
                              <ion-row style="margin: 0px; padding: 0px; border-bottom: 1px dotted grey">
                                <ion-col style="margin: 0px; padding: 0px" size="12">
                                `+alamat+`
                                </ion-col>
                              </ion-row>
                              <ion-row style="margin: 0px; padding: 0px; border-bottom: 1px dotted grey">
                                <ion-col style="margin: 0px; padding: 0px" size="12">
                                  Distance `+Math.ceil(distance)+` KM from your current location
                                </ion-col>
                              </ion-row>
                              <ion-row style="margin: 0px; padding: 0px; border-bottom: 1px dotted grey">
                                <ion-col style="margin: 0px; padding: 0px" size="12">
                                  Cosine : </br>`+cosine+`
                                </ion-col>
                              </ion-row>
                              <ion-row style="margin: 0px; padding: 0px; border-bottom: 1px dotted grey">
                                <ion-col style="margin: 0px; padding: 0px" size="12">
                                  Gower : </br>`+gower+`
                                </ion-col>
                              </ion-row>
                              <ion-row style="margin: 0px; padding: 0px; border-bottom: 1px dotted grey">
                                <ion-col style="margin: 0px; padding: 0px" size="12">
                                  Location : </br>`+longitude+`/`+latitude+`
                                </ion-col>
                              </ion-row>
                            </ion-grid>
                          </ion-card-content>
                          <ion-footer class="ion-no-border">
                            <ion-toolbar>
                              <center><ion-router-link href="/detailmakanan/1/`+id_profil_makanan+`" class="color">Pilih</ion-router-link></center>
                            </ion-toolbar>
                          </ion-footer>
                        </ion-card>`;
                      }
                      document.getElementById("rekomendasi").innerHTML = htmlcode;
                      loading.dismiss();
                    },
                    async () => {
                      const toast = await this.toastCtrl.create({message: 'Check your connection.', duration: 2000, color: 'dark'});
                      await toast.present();
                      loading.dismiss();
                    }
                  );
                },
                async () => {
                  const toast = await this.toastCtrl.create({message: 'Check your connection.', duration: 2000, color: 'dark'});
                  await toast.present();
                  loading.dismiss();
                }
              );
            },
            async () => {
              const toast = await this.toastCtrl.create({message: 'Check your connection.', duration: 2000, color: 'dark'});
              await toast.present();
              loading.dismiss();
            }
          );

        });
    }

  }

}
