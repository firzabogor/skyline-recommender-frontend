import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DashboardcustomerPage } from './dashboardcustomer.page';

describe('DashboardcustomerPage', () => {
  let component: DashboardcustomerPage;
  let fixture: ComponentFixture<DashboardcustomerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardcustomerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DashboardcustomerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
