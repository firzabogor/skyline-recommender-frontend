import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreferensiPage } from './preferensi.page';

const routes: Routes = [
  {
    path: '',
    component: PreferensiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreferensiPageRoutingModule {}
