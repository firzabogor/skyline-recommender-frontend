import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-preferensi',
  templateUrl: './preferensi.page.html',
  styleUrls: ['./preferensi.page.scss'],
})
export class PreferensiPage implements OnInit {

  constructor(
    private authService: AuthService,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private router: Router
  ) { }

  ngOnInit() {
  }

  form = new FormGroup({
    id_type_makanan: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    id_customer: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    bahan_bumbu: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    tekstur: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    aroma: new FormControl('', [
      Validators.required,
    ]),
    rasa: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    cara_memasak: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    cara_penyajian: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    level_pedas: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    level_gurih: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    level_asam: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    level_manis: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    level_asin: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    nutrisi: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    tekstur_level: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    penggunaan_bahan_alami: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    kesegaran_makanan: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    kebersihan_makanan: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    status_halal_makanan: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    harga: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
  });

  async lewati(){
    let data = JSON.parse(localStorage.getItem("data"));
    let customer = JSON.parse(localStorage.getItem("customer"));
    this.authService.lewati(customer[0]['kode_asal']).subscribe(
      async values => {
        data = values['values']['baseline_makanan'];
        var i;
        for(i = 0; i<data.length; i++){
          data[i]['id_customer'] = customer[0]['id'];
          this.authService.preferensi(data[i]).subscribe(
            async () => {
              this.router.navigateByUrl('/dashboardcustomer');
            },
            async () => {
              console.log('gagal');
            },
          );
        }
      },
      async () => {
        console.log("Gagal");
      }
    );

  }

  async onSubmit(){
    const loading = await this.loadingCtrl.create({message: 'Logging...'});
    await loading.present();
    let customer = JSON.parse(localStorage.getItem("customer"));
    this.form.value.id_customer = customer['id'];
    this.authService.preferensi(this.form.value).subscribe(
      async () => {
        loading.dismiss();
        this.router.navigateByUrl('/home');
      },
      async () => {
        const alert = await this.alertCtrl.create({message: 'Failed', buttons: ['ok']});
        loading.dismiss();
        await alert.present();
      },
    );
  }

}
